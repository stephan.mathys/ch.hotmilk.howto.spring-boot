**Overall**
This project is a sample how spring boot, gradle and gitlab work together.

Project start

- Clone git project
- Change directory to project "ch.hotmilk.howto.spring-boot"
- Run command "./gradlew bootRun"
- It takes a while but after all the project should be up and running
- Try out the following links:  
http://localhost:8080  
http://localhost:8080/helloAgain  
http://localhost:8080/helloAgain?data=myBigWorld  

**Development with eclipse**

- Import the project into workspace (Git perspective - rightclick at Working Tree directory and import eclipse project)
- Find Applicaton class and click run

Maybe you have to download the gradle eclipse gradle plugin