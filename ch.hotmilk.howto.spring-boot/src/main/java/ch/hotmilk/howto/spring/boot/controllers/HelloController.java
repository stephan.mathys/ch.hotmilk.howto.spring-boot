package ch.hotmilk.howto.spring.boot.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class HelloController {

    private final Logger logger = LoggerFactory.getLogger(HelloController.class);

    @GetMapping({ "/", "/hello" })
    public String index(Model model,
            @RequestParam(value = "name", required = false, defaultValue = "World") String name) {
        logger.info("index had been called");
        model.addAttribute("name", name + "?");
        return "hello";
    }
}
