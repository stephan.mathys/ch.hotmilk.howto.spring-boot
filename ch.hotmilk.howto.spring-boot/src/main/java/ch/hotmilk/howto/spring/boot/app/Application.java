package ch.hotmilk.howto.spring.boot.app;

import java.util.Arrays;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages = { "ch.hotmilk.howto.spring.boot.controllers" })
@ComponentScan(basePackages = { "ch.hotmilk.howto.spring.boot.controllers.rest" })

@SpringBootApplication
public class Application {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    /**
     * This is a Bean for print out all the the Beans in the application
     **/
    @Bean
    public CommandLineRunner commandLineRunner(ApplicationContext ctx) {
        return args -> {

            System.out.println("Let's inspect the beans provided by Spring Boot:");

            String[] beanNames = ctx.getBeanDefinitionNames();
            Arrays.sort(beanNames);
            for (String beanName : beanNames) {
                if (beanName.equalsIgnoreCase("HelloAgainController")) {
                    System.out.println("***************************************************************");
                    System.out.println(beanName);
                    System.out.println("***************************************************************");
                } else {
                    System.out.println(beanName);
                }
            }

        };
    }

}
