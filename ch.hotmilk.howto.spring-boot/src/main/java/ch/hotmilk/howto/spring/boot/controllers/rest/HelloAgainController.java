package ch.hotmilk.howto.spring.boot.controllers.rest;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloAgainController {

    @GetMapping("/helloAgain")
    public @ResponseBody String myHelloAgainMesssage(
            @RequestParam(value = "data", required = false, defaultValue = "myWorld") String paramData) {
        return "Hello Again " + paramData;
    }
}
