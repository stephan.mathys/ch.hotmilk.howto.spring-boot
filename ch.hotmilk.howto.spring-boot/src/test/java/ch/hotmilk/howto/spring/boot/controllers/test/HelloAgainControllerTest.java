package ch.hotmilk.howto.spring.boot.controllers.test;

import static org.hamcrest.Matchers.equalTo;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import ch.hotmilk.howto.spring.boot.app.Application;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class)
@AutoConfigureMockMvc
public class HelloAgainControllerTest {

	@Autowired
	private MockMvc mvc;

	@Test
	public void getHelloAgainWithoutParam() throws Exception {
		mvc.perform(MockMvcRequestBuilders.get("/helloAgain").accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk()).andExpect(content().string(equalTo("Hello Again myWorld")));
	}

	@Test
	public void getHelloAgainWithParam() throws Exception {
		mvc.perform(MockMvcRequestBuilders.get("/helloAgain?data=myName").accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk()).andExpect(content().string(equalTo("Hello Again myName")));
	}
}
